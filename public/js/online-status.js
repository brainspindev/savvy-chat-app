function nextStatus(statusElement) {
  console.log(statusElement.className);
  switch (statusElement.className) {
    case "online-status is-online":
      statusElement.className = "online-status is-offline";
      break;
    case "online-status is-offline":
      statusElement.className = "online-status is-away";
      break;
    case "online-status is-away":
      statusElement.className = "online-status is-busy";
      break;
    case "online-status is-busy":
      statusElement.className = "online-status is-error";
      break;
    case "online-status is-error":
      statusElement.className = "online-status is-online";
      break;
  }
}
