$(document).ready(function(){
  $(".btn-call").click(function(){
    if($(this).hasClass("btn-primary")) {
      $(this).children("span").text("End call with John");
      $(".call-notes").show("fast");
    } else {
      $(this).children("span").text("Call John for weekly coaching");
      $(".call-notes").hide("fast");
    }
    $(this).toggleClass("btn-primary");
    $(this).toggleClass("btn-danger");
  });
});
