// ---- Combined JS for dashboard & mobile ---- //

// ** Ignore on dashboard
var mobileMessageArea = document.getElementById("message-area");

// ** Ignore on mobile
var dashboardMessageAreas = document.getElementsByClassName("message-area");

// Get all of the elements with autogrow class
var autogrowAreas = document.getElementsByClassName("autogrow");

// ** Ignore this on mobile
// Get the hidden tabs to be able to change them
var hiddenTabs = document.getElementsByClassName("tab-pane");

// ** Ignore on dashboard
// Helper function for auto sizing the area above the chat bubble
/**
 * Get the closest matching element up the DOM tree.
 * https://gomakethings.com/climbing-up-and-down-the-dom-tree-with-vanilla-javascript/
 * @private
 * @param  {Element} elem     Starting element
 * @param  {String}  selector Selector to match against
 * @return {Boolean|Element}  Returns null if not match found
 */
var getClosest = function ( elem, selector ) {

  // Element.matches() polyfill
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;
      };
  }
  // Get closest match
  for ( ; elem && elem !== document; elem = elem.parentNode ) {
    if ( elem.matches( selector ) ) return elem;
  }
  return null;
};

// ** Ignore on dashboard
var optionContainer = getClosest(autogrowAreas[0].parentNode, '#options');
// ** Ignore on dashboard
var bubbleContainer = getClosest(autogrowAreas[0].parentNode, '.bubble');

function updateScroll(element){
  if(element) {
    element.scrollTop = element.scrollHeight - element.clientHeight;
  }
}

function resize(autogrowElement){

  if(!autogrowElement) {
    return
  }
  
  var children = autogrowElement.parentNode.childNodes;

  autogrowElement.style.height = 'auto';
  autogrowElement.style.height = autogrowElement.scrollHeight+'px';

  // ** Ignore on mobile, unless mobile is going to have a type-ahead as well
  // Position the type-ahead from the bottom based on the textarea's height
  for (x = 0, len = children.length; x < len; x++) {
    if(children[x].className == "type-ahead"){
      if(autogrowElement.scrollHeight >= 200){ // the text-area's max-height
        children[x].style.bottom = "216px";
      } else {
        children[x].style.bottom = autogrowElement.scrollHeight + 16 + "px";
      }
    }
  }

  // ** Ignore on dashboard
  // Check if it has a parent with id #options and then add the scrollHeight to the top margin
  if(optionContainer && bubbleContainer) {
    optionContainer.style.marginTop = bubbleContainer.scrollHeight - 10 + "px";
  }
  // ** Ignore on dashboard
  updateScroll(mobileMessageArea);

  // ** Ignore on mobile
  // Loop to scroll to the bottom of each message area
  for (x = 0, len = dashboardMessageAreas.length; x < len; x++) { // I used "x" as the variable here because using "i" broke it
    updateScroll(dashboardMessageAreas[x]);
  }
}

// ** Ignore this on mobile
// Loop to make the hidden tabs displayed but not visible
for (i = 0, len = hiddenTabs.length; i < len; i++) {
  hiddenTabs[i].style.visibility = "hidden";
  hiddenTabs[i].style.display = "block";
}

// Loop to resize each autogrow area
for (i = 0, len = autogrowAreas.length; i < len; i++) {
  resize(autogrowAreas[i]);
}

// ** Ignore on dashboard
// Scroll to the bottom of message area when first loaded
updateScroll(mobileMessageArea);

// ** Ignore on mobile
// Loop to scroll to the bottom of each message area when first loaded
for (i = 0, len = dashboardMessageAreas.length; i < len; i++) {
  updateScroll(dashboardMessageAreas[i]);
}

// ** Ignore this on mobile
// Loop to make the hidden tabs back to normal
for (i = 0, len = hiddenTabs.length; i < len; i++) {
  hiddenTabs[i].style.visibility = null;
  hiddenTabs[i].style.display = null;
}

//Set event listeners to resize autogrow areas on input
for (i = 0, len = autogrowAreas.length; i < len; i++) {
  if (autogrowAreas[i].addEventListener) {
    autogrowAreas[i].addEventListener('input', function() {

      resize(this);

    }, false);
  }
}
// If you need this to work with IE then look at the second answer here: http://stackoverflow.com/questions/2823733/textarea-onchange-detection
