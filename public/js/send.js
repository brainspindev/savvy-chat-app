$(document).ready(function(){
  var oldVal = "";
  $("textarea").on("change keyup paste", function() {
      var currentVal = $(this).val();
      if(currentVal == oldVal) {
          return; //check to prevent multiple simultaneous triggers
      }
      oldVal = currentVal;
      if($(this).val()) {
        if(!$(this).hasClass("has-content")) {
          $(this).addClass("has-content");
        }
      } else {
        if($(this).hasClass("has-content")) {
          $(this).removeClass("has-content");
        }
      }
  });
});
